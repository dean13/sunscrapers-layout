var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps');
    uglify = require('gulp-uglify');
    cleanCSS = require('gulp-clean-css');
    minify = require('gulp-minify');
    runSequence = require('run-sequence');

/** Compile and minify scss to css */
gulp.task('min-css', function () {
    return gulp.src('style/style.scss')
        .pipe(sourcemaps.init()) 
        .pipe(sass())
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest('dist/style'));
});

/** minify js */
gulp.task('min-js', function() {
  gulp.src('js/*.js')
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('dist/js'))
});

/** Run all tasks to compile and minify files */
gulp.task('develop', function(done) {
    runSequence('min-css', 'min-js', function() {
        console.log('Project building success');
        done();
    });
});

/** watch changes in scss file */
gulp.task('sass:watch', function () {
    gulp.watch('./style/style.scss', ['sass']);
});