$(document).ready(function () {

    $(".toggle-menu").on('click', function () {
        var $this = $(this);

        $this.toggleClass('active');

        if ($this.is('.active')) {
            $('.nav-left').show();
            $('.right-menu').addClass('show');
        } else {
            $('.nav-left').hide();
            $('.right-menu').removeClass('show');
        }
    });

});